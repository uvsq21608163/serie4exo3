#ifndef CLIST_H
#define CLIST_H
#include <list>
#include <iostream>
using namespace std;
template<class T>
class CList
{
    public:

        CList():l(){};
        /*virtual ~CList();*/
        void operator>(T& i){

				if (l.size() != 0){
					typename list<T>::iterator it = l.begin();
					i = *it;
					it = l.erase(it);
				}

			}
        template<class V>
        friend ostream& operator<<(ostream&, CList<V>&);
        virtual CList<T>& ajouter(const T&) =0;
        template<class U>
        friend CList<U>& operator<(CList<U>&, U);
        list<T> getList(){ return l;};
    	list<T> l;
};

template<class T>
ostream& operator<<(ostream& flux,CList<T>& maliste){
	for(typename list<T>::iterator it = maliste.l.begin();
		it != maliste.l.end();
		++it){
		flux<<*it<<'\t';
	}
	return flux;
}
template<class T>
CList<T>& operator<(CList<T>& L, T i){
 	L.ajouter(i);
 	return L;
 }
#endif // CLIST_H
